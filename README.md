# Applanist

Web planner with todos, memos and tracking goals. It's a full-stack application.

## Stack
* JavaScript
* Node
* React - v4.0.0
* Redux - v4.0.0
* MongoDB - v3.1.10
* webpack - v4.17.0
* Babel - v7.0.0
* Saga

## Setup
```
cd applanist-site

npm install

npm run dev
```
